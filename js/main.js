$.fn.modal = function(method) {
	var $btnClose = $(this).find('.btn-close');

	var close = function() {
		$('body').removeClass('modal-visible');
	}

	var open = function() {
		$('body').addClass('modal-visible');
	}

	if(method === 'close') {
		close();
	}

	if(method === 'open') {
		open();
	}

	$btnClose.on('click', function(e) {
		close();
		e.preventDefault();
	});

	return this;
}

$.fn.progressBar = function(values) {
    var self = this,
        $progressEle = $(this).find('.progress').width('0%'),
        $percentEle = $(this).find('.percent'),
        options = {
            start: 0,
            finish: 100,
            duration: 1000,
            finshedClass: 'finished'
        }
    
    $(self).removeClass(options.finshedClass);
    
    $.extend(options, values);
    
    /** updateProgress - time based percentage modification */
    var updateProgress = function() {
        
        var startTime = new Date().getTime(), // get current time
            amountDiff = options.finish - options.start; // get remaining percentage
        
        var timer = setInterval(function() {
            var now = new Date().getTime(),
                diff = now - startTime, // get elapsed milliseconds from startTime
                timePercent = (diff / options.duration), // calculate percentage of time elapsed
                actualPercent = timePercent * amountDiff + options.start, // add the increased/additional percentage amount on top of the starting percentage
                intPercent = Math.floor(Math.min(actualPercent, 100));
            
            $progressEle.css('width', intPercent + '%');
            $percentEle.text(intPercent);
            
            if(timePercent >= 1) {
                clearInterval(timer);
                $(self).addClass(options.finshedClass);
                 var endTime = new Date().getTime();
                console.log(endTime - startTime);
            }
            
        }, 20);
        
    }();
    
}

var TWA = {};

TWA.fetchData = function(uri, cb) {
	$.getJSON(uri, function(result) {
		cb(result.data);
	});
}

$(function() {

    var applyProgress = function() {
        
        TWA.fetchData('/js/data.json', function(data) {
            $('#progress-main').progressBar(data.lightbox);
            $('#modal').modal('open');
        });
    };
	
    $('#btn-reset').on('click', function(e) {
        e.preventDefault();
        $(this).blur();
        applyProgress();
    });
    
    applyProgress();

});